import face_recognition
from PIL import Image, ImageDraw
import cv2
from skimage import io, img_as_float
import numpy as np

# Open the input movie file
input_movie = cv2.VideoCapture("Data/s2_l_bbim3a.mov")
length = int(input_movie.get(cv2.CAP_PROP_FRAME_COUNT))

# Initialize some variables
face_locations = []
face_encodings = []
face_names = []
frame_number = 0

while True:

    # Grab a single frame of video
    ret, frame = input_movie.read()
    frame_number += 1

    # Quit when the input video file ends
    if not ret:
        break

    # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
    rgb_frame = frame[:, :, ::-1]

    # Find all facial features in all the faces in the image
    face_landmarks_list = face_recognition.face_landmarks(rgb_frame)

    for face_landmarks in face_landmarks_list:

        left_x = face_landmarks['chin'][2][0]
        right_x = face_landmarks['chin'][13][0]
        top_y = face_landmarks['chin'][2][1]
        bottom_y = face_landmarks['chin'][8][1]

        mid_x = left_x + right_x / 2
        mid_y = top_y + bottom_y / 2

        image = img_as_float(rgb_frame)
        
        sum_x = map(lambda x: (x - mid_x)**2, face_landmarks['top_lip'][:][0] + face_landmarks['bottom_lip'][:][0])
        sum_y = map(lambda y: (y - mid_y)**2,  face_landmarks['top_lip'][:][1] + face_landmarks['bottom_lip'][:][1])
        dist  = sum(map(lambda x, y: (x + y) ** 0.5, sum_x, sum_y))

        print('Dist', dist)

print(frame_number)
# All done!
input_movie.release()
cv2.destroyAllWindows()