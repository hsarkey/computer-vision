'''
A Recurrent Neural Network (LSTM) implementation example using TensorFlow library.
This example is using the MNIST database of handwritten digits (http://yann.lecun.com/exdb/mnist/)
Long Short Term Memory paper: http://deeplearning.cs.cmu.edu/pdfs/Hochreiter97_lstm.pdf

Based on: https://github.com/aymericdamien/TensorFlow-Examples/
'''

from __future__ import print_function

import tensorflow as tf
from tensorflow.contrib import rnn 
import numpy as np
import os
import object_oriented

import face_recognition
from PIL import Image, ImageDraw
import cv2
from skimage import io, img_as_float
import numpy as np
import os

import scipy.interpolate as interp

class dataPoint:

    def __init__(self, file_name):
        self.input_movie = cv2.VideoCapture(file_name)
        self.length = int(self.input_movie.get(cv2.CAP_PROP_FRAME_COUNT))

        # Initialize some variables
        #self.geometryFeatures =  GeometryFeatures(self.input_movie)      
        self.intensityFeatures = IntensityFeatures(file_name)

class GeometryFeatures: 

    def __init__(self, input_movie):

        # Open the input movie file
        self.input_movie = input_movie
        self.length = int(self.input_movie.get(cv2.CAP_PROP_FRAME_COUNT))

        # Initialize some variables
        self.frame_number = 0
        self.featureVector = []

        self.getGeo()

    def getGeo(self):

        while True:

            # Grab a single frame of video
            ret, frame = self.input_movie.read()
            self.frame_number += 1

            # Quit when the input video file ends
            if not ret:
                break

            # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
            rgb_frame = frame[:, :, ::-1]

            # Find all facial features in all the faces in the image
            face_landmarks_list = face_recognition.face_landmarks(rgb_frame)

            for face_landmarks in face_landmarks_list:

                left_x = face_landmarks['chin'][2][0]
                right_x = face_landmarks['chin'][13][0]
                top_y = face_landmarks['chin'][2][1]
                bottom_y = face_landmarks['chin'][8][1]

                mid_x = left_x + right_x / 2
                mid_y = top_y + bottom_y / 2

                image = img_as_float(rgb_frame)
                
                dist = list(map(lambda t: ((t[0] - mid_x)**2 + (t[1]-mid_y)**2)**0.5, face_landmarks['top_lip'][:] + face_landmarks['bottom_lip'][:]))
                self.featureVector.append(dist)

        self.input_movie.release()
        cv2.destroyAllWindows()


class IntensityFeatures:

    def __init__(self, file_name):
    
        # Open the input movie file
        self.file_name = file_name
        self.input_movie = cv2.VideoCapture(file_name)
        self.length = int(self.input_movie.get(cv2.CAP_PROP_FRAME_COUNT))

        # Initialize some variables
        self.frame_number = 0
        self.featureVector = []
        self.getIntensity()

        self.size = len(self.featureVector)

    def printIntensitySizes(self):
        print("Feature vector: {}, single feature: {}", len(self.featureVector), len(self.featureVector[0]))

    def printIntensity(self):

        print("Intensity features for: ", self.file_name)
        for feature in self.featureVector:
            for row in feature:
                print(row)
            print("\n")

    def getIntensity(self):
        
        while True:
            # Grab a single frame of video
            ret, frame = self.input_movie.read()
            self.frame_number += 1

            # Quit when the input video file ends
            if not ret:
                break

            # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
            rgb_frame = frame[:, :, ::-1]
            # pil_image = Image.fromarray(rgb_frame)
            # d = ImageDraw.Draw(pil_image)

            # Find all facial features in all the faces in the image
            face_landmarks_list = face_recognition.face_landmarks(rgb_frame)
            for face_landmarks in face_landmarks_list:
                left_x = face_landmarks['chin'][2][0]
                right_x = face_landmarks['chin'][13][0]
                top_y = face_landmarks['chin'][2][1]
                bottom_y = face_landmarks['chin'][8][1]

                size = 8 
                width  = abs(right_x - left_x) // size
                height = abs(bottom_y - top_y) // size

                # d.rectangle([(left_x, top_y), (right_x, bottom_y)])

                # for i in range(size):
                #     d.line([(left_x + width*i, top_y), (left_x + width*i, bottom_y) ])
                #     d.line([(left_x, top_y + height*i), (right_x, top_y + height*i)])

                avg_intensity = [ [0 for i in range(size)] for j in range(size)]

                image = img_as_float(rgb_frame)
                # print(len(image), len(image[0]))
                
                for j in range(size):
                    for i in range(size):
                        avg_intensity[i][j] = np.mean(image[top_y + height*j:top_y + height*(j + 1), left_x + width*i:left_x + width*(i + 1)])
                a = np.array(avg_intensity)
                self.featureVector.append(a.flatten())

                # pil_image.show()

        cv2.destroyAllWindows()

    def interpolate(self, n):

        ln = len(self.featureVector)
        diff = ln - n

        if not diff:
            return

        v = self.featureVector
        newArr = [[] for i in range(n)]

        print("n: {}, size: {}, diff: {}, len: {}".format(n, self.length, diff, len(v)))
        
        if diff > 0: 
            count = 0
            index = n // diff
            i     = 0

            if diff == 1:
                index = n // 2

            skipped = 0
            while count < n:
                print("count: {}, i: {} index: {}".format(count, i, index)) 
                if count and not count % index and skipped < diff:
                    skipped += 1
                    i += 1
                newArr[count] = v[i]
                count += 1
                i += 1

        else: 
            count = 0
            index = ln // diff
            i     = 0

            if diff == -1:
                index = n // 2

            diff = diff * -1 
            replaced = 0
            while count < n:
                print("count: {}, i: {} index: {}".format(count, i, index)) 
                if not count % index and replaced < diff:
                    if (i + 1) < ln: 
                        newArr[count] = np.mean( np.array([ v[i + 1], v[i]]), axis=0 )
                    else:
                        newArr[count] = np.mean( np.array([ v[i - 1], v[i]]), axis=0 )
                    print("replaced one")
                    count += 1
                    replaced += 1
                else: 
                    newArr[count] = v[i]
                    count += 1
                    i += 1
                

        self.featureVector = newArr


'''
To classify images using a recurrent neural network, we consider every image
row as a sequence of pixels. Because MNIST image shape is 28*28px, we will then
handle 28 sequences of 28 steps for every sample.
'''

def RNN(x, weights, biases):

    # Prepare data shape to match `rnn` function requirements
    # Current data input shape: (batch_size, n_steps, n_input)
    # Required shape: 'n_steps' tensors list of shape (batch_size, n_input)

    # Permuting batch_size and n_steps
    x = tf.transpose(x, [1, 0, 2])

    # Reshaping to (n_steps*batch_size, n_input)
    x = tf.reshape(x, [-1, n_input])

    # Split to get a list of 'n_steps' tensors of shape (batch_size, n_input)
    #x = tf.split(0, n_steps, x)
    x = tf.split(x, n_steps, 0)

    # Define a lstm cell with tensorflow
    # Just basic RNN cell
    cell = rnn.BasicRNNCell(n_hidden)

    # LSTM
    # cell = rnn_cell.BasicLSTMCell(n_hidden, forget_bias=1.0)

    # Use Peephole
    # cell = rnn_cell.LSTMCell(n_hidden, use_peepholes=True)

    # Output without activation
    # from layers import MyLSTMCell
    # cell = MyLSTMCell(n_hidden)

    # GRU
    # cell = rnn_cell.GRUCell(n_hidden)

    # Or if you want something else, just modify the __call__ function in layers.MyLSTMCell

    # Get lstm cell output
    outputs, states = rnn.static_rnn(cell, x, dtype=tf.float32)

    # Linear activation, using rnn inner loop last output
    return tf.matmul(outputs[-1], weights['out']) + biases['out']

if __name__ == "__main__":

    # directory = r'./small-data'
    # data_points = [ ]

    # for filename in os.listdir(directory):
    #     if filename.endswith(".mp4"):
    #         d = dataPoint(filename)
    #         data_points.append(d)
    #     else:
    #         continue


    # d.intensityFeatures.interpolate(12)
    # d.intensityFeatures.printIntensity()

    directory = r'./small-data'
    data_points = [ ]
    labels      = [ ]
    sizes       = [ ]
    file_names  = [ ]

    # 1: PB, 2: SG, 3: LR

    for filename in os.listdir(directory):
        if filename.endswith(".mp4"):
            print("working with: {}".format(filename))
            if filename.endswith("lr.mp4"):
                labels.append(3)
            elif filename.endswith("sg.mp4"):
                labels.append(2)
            elif filename.endswith("pb.mp4"):
                labels.append(1)
            d = dataPoint(directory + '/' + filename)
            data_points.append(d)
            sizes.append(d.intensityFeatures.size)
            file_names.append(filename)
        else:
            continue
        print("done with: {}".format(filename))

    sizes = np.array(sizes)
    avg_frames = int(np.average(sizes))
    
    for i, d in enumerate(data_points):
        print("Interpolating {} of size {} to {} frames".format(file_names[i], sizes[i], avg_frames))
        d.intensityFeatures.interpolate(avg_frames)
        print("Done")

    # Parameters
    learning_rate = 0.001
    training_iters = 100000
    batch_size = 1
    display_step = 10

    # Network Parameters
    n_input = 64 # MNIST data input (img shape: 28*28)
    n_steps = avg_frames # timesteps
    n_hidden = 128 # hidden layer num of features
    n_classes = 3 # MNIST total classes (0-9 digits)
    data_point_num = 0
    # d.intensityFeatures.printIntensity()
    # d.intensityFeatures.printIntensitySizes()
    x = tf.constant(np.array([d.intensityFeatures.featureVector for d in data_points]))
    x = tf.cast(x,tf.float32)
    y = tf.constant([1, 2, 3])

    # Define weights
    weights = {
        'out': tf.Variable(tf.random_normal([n_hidden, n_classes]))
    }
    biases = {
        'out': tf.Variable(tf.random_normal([n_classes]))
    }

    # X has all of the test data
    # Pred has all of the responses

    pred = RNN(x, weights, biases)

    # Define loss and optimizer
    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=pred, labels=labels))
    tvars = tf.trainable_variables()

    # Error: 
    grads, _ = tf.clip_by_global_norm(tf.gradients(cost, tvars), 5.0)
    optimizer = tf.train.AdamOptimizer(learning_rate).apply_gradients(zip(grads, tvars))

    # Evaluate model
    correct_pred = tf.equal(tf.argmax(pred,1), tf.argmax(y,1))
    accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

    # Initializing the variables
    init = tf.initialize_all_variables()

    data_point_num = 0
    # Launch the graph
    with tf.Session() as sess:
        sess.run(init)
        # Keep training until reach max iterations
        while data_point_num < len(data_points):
            batch_x = data_points[data_point_num:data_point_num+batch_size]
            batch_y = labels[data_point_num:data_point_num+batch_size]
            data_point_num += batch_size
            # Reshape data to get 28 seq of 28 elements
            batch_x = batch_x.reshape((batch_size, n_steps, n_input))
            # Run optimization op (backprop)
            sess.run(optimizer, feed_dict={x: batch_x, y: batch_y}) # Test: don't run this 
            if data_point_num % display_step == 0:
                # Calculate batch accuracy
                acc = sess.run(accuracy, feed_dict={x: batch_x, y: batch_y}) 
                # Calculate batch loss
                loss = sess.run(cost, feed_dict={x: batch_x, y: batch_y})
                print("Iter " + str(step*batch_size) + ", Minibatch Loss= " + \
                    "{:.6f}".format(loss) + ", Training Accuracy= " + \
                    "{:.5f}".format(acc))
            data_point_num += 1
        print("Optimization Finished!")

        # Calculate accuracy for 128 mnist test images
        # test_len = 128
        # test_data = mnist.test.images[:test_len].reshape((-1, n_steps, n_input))
        # test_label = mnist.test.labels[:test_len]
        # print("Testing Accuracy:", \
        #     sess.run(accuracy, feed_dict={x: test_data, y: test_label}))