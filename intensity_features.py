import face_recognition
from PIL import Image, ImageDraw
import cv2
from skimage import io, img_as_float
import numpy as np

# Open the input movie file
input_movie = cv2.VideoCapture("Data/s2_l_bbim3a.mov")
length = int(input_movie.get(cv2.CAP_PROP_FRAME_COUNT))

# Create an output movie file (make sure resolution/frame rate matches input video!)
fourcc = cv2.VideoWriter_fourcc(*'XVID')
output_movie = cv2.VideoWriter('output.avi', fourcc, 29.97, (640, 360))

# Initialize some variables
face_locations = []
face_encodings = []
face_names = []
frame_number = 0

while True:
    # Grab a single frame of video
    ret, frame = input_movie.read()
    frame_number += 1

    if frame_number > 3:
        break

    # Quit when the input video file ends
    if not ret:
        break

    # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
    rgb_frame = frame[:, :, ::-1]

    # Find all facial features in all the faces in the image
    face_landmarks_list = face_recognition.face_landmarks(rgb_frame)

    for face_landmarks in face_landmarks_list:

        left_x = face_landmarks['chin'][2][0]
        right_x = face_landmarks['chin'][13][0]
        top_y = face_landmarks['chin'][2][1]
        bottom_y = face_landmarks['chin'][8][1]

        size = 8 
        width  = abs(right_x - left_x) // size
        height = abs(bottom_y - top_y) // size
        avg_intensity = [ [0 for i in range(size)] for j in range(size)]


        image = img_as_float(rgb_frame)
        print(np.mean(image))  
        
        for j in range(size):
            for i in range(size):
                avg_intensity[i][j] = np.mean(image[left_x + width*i:left_x + width*(i + 1),top_y + height*i:top_y + height*(i + 1)])

        print('Avg Intensity:', avg_intensity)
            

    # Find average intensity



    print((left_x, top_y), (right_x, bottom_y), face_landmarks['chin'][14])




# All done!
input_movie.release()
cv2.destroyAllWindows()