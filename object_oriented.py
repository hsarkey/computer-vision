import face_recognition
from PIL import Image, ImageDraw
import cv2
from skimage import io, img_as_float
import scipy.interpolate as interp
import numpy as np
import os

FRAME_SIZE = 20

class dataPoint:

    def __init__(self, file_name):
        self.input_movie = cv2.VideoCapture(file_name)
        self.length = int(self.input_movie.get(cv2.CAP_PROP_FRAME_COUNT))

        # Initialize some variables
        self.geometryFeatures =  GeometryFeatures(self.input_movie)      
        self.intensityFeatures = IntensityFeatures(file_name)

    def interpolate(self, n):
    
        x = np.arange(0, n)
        y = np.array(self.geometryFeatures) 
        print(self.geometryFeatures)
        print(x.ndim)
        print(y.ndim)
        f = interp.interp1d(x, y)

        return f

class GeometryFeatures: 

    def __init__(self, input_movie):

        # Open the input movie file
        self.input_movie = input_movie
        self.length = int(self.input_movie.get(cv2.CAP_PROP_FRAME_COUNT))

        # Initialize some variables
        self.frame_number = 0
        self.featureVector = []

        self.getGeo()

    def getGeo(self):

        while True:

            # Grab a single frame of video
            ret, frame = self.input_movie.read()
            self.frame_number += 1

            # Quit when the input video file ends
            if not ret:
                break

            # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
            rgb_frame = frame[:, :, ::-1]

            # Find all facial features in all the faces in the image
            face_landmarks_list = face_recognition.face_landmarks(rgb_frame)

            for face_landmarks in face_landmarks_list:

                left_x = face_landmarks['chin'][2][0]
                right_x = face_landmarks['chin'][13][0]
                top_y = face_landmarks['chin'][2][1]
                bottom_y = face_landmarks['chin'][8][1]

                mid_x = left_x + right_x / 2
                mid_y = top_y + bottom_y / 2

                image = img_as_float(rgb_frame)
                
                dist = list(map(lambda t: ((t[0] - mid_x)**2 + (t[1]-mid_y)**2)**0.5, face_landmarks['top_lip'][:] + face_landmarks['bottom_lip'][:]))
                self.featureVector.append(dist)

        self.input_movie.release()
        cv2.destroyAllWindows()


class IntensityFeatures:

    def __init__(self, file_name):
    
        # Open the input movie file
        self.file_name = file_name
        self.input_movie = cv2.VideoCapture(file_name)
        self.length = int(self.input_movie.get(cv2.CAP_PROP_FRAME_COUNT))

        # Initialize some variables
        self.frame_number = 0
        self.featureVector = []

    def printIntensity(self):

        print("Intensity features for: ", self.file_name)
        for feature in self.featureVector:
            for row in feature:
                print(row)
            print("\n")

    def getIntensity(self):
        
        while True:
            # Grab a single frame of video
            ret, frame = self.input_movie.read()
            self.frame_number += 1

            # Quit when the input video file ends
            if not ret:
                break

            if self.frame_number > 3:
                break

            # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
            rgb_frame = frame[:, :, ::-1]
            # pil_image = Image.fromarray(rgb_frame)
            # d = ImageDraw.Draw(pil_image)

            # Find all facial features in all the faces in the image
            face_landmarks_list = face_recognition.face_landmarks(rgb_frame)
            for face_landmarks in face_landmarks_list:
                left_x = face_landmarks['chin'][2][0]
                right_x = face_landmarks['chin'][13][0]
                top_y = face_landmarks['chin'][2][1]
                bottom_y = face_landmarks['chin'][8][1]

                size = 8 
                width  = abs(right_x - left_x) // size
                height = abs(bottom_y - top_y) // size

                # d.rectangle([(left_x, top_y), (right_x, bottom_y)])

                # for i in range(size):
                #     d.line([(left_x + width*i, top_y), (left_x + width*i, bottom_y) ])
                #     d.line([(left_x, top_y + height*i), (right_x, top_y + height*i)])

                avg_intensity = [ [0 for i in range(size)] for j in range(size)]

                # image = img_as_float(rgb_frame)
                # print(len(image), len(image[0]))
                
                for j in range(size):
                    for i in range(size):
                        avg_intensity[i][j] = np.mean(image[top_y + height*j:top_y + height*(j + 1), left_x + width*i:left_x + width*(i + 1)])
                        print("bounds:", left_x + width*i, left_x + width*(i + 1), top_y + height*j, top_y + height*(j + 1))
                
                self.featureVector.append(avg_intensity)

                # pil_image.show()

        cv2.destroyAllWindows()

if __name__ == "__main__":
    
    # file_name = "person2_lr.mp4"
    # intense = IntensityFeatures(file_name)
    # intense.getIntensity()
    # intense.printIntensity()

    directory = r'./small-data'
    data_points = [ ]

    for filename in os.listdir(directory):
        if filename.endswith(".mp4"):
            d = dataPoint(filename)
            data_points.append(d)
        else:
            continue

    d.interpolate(20)
    print(d.geometryFeatures)